# 🚀 How to Set Up RocketChat Settings for OMC
This document describes the changes that were made to the default RocketChat settings in the admin panel, so the same steps can be taken again if the OMC community ever needs to be recreated.

## ✅ Prerequisites
This guide assumes that you have already done the following:
- You have a server purchased. (eg: [SporeStack](https://sporestack.com/))
- You have a domain name purchased, and have the ability to set up subdomains.
- You know how to ssh into your server, and test the subdomain set up.
- You have RocketChat installed using Docker.

## 🛠️ Launch Your Workspace
- **1/4 Admin Info**
  - _Display Name_
  - _username_
  - _email_
  - _password_

[img]

- **2/4 Organization Info**
  - `Open MAP Community`
  - Social Network
  - 101-250 people
  - Sweden

[img]

- **3/4 Register your workspace**
  - _email_ (Do not use the same email more than once for RocketChat, I run into some errors verifying my registration because of it.)
  - I agree

[img]

- **4/4 Awaiting confirmation**
  - (Check your email Inbox or Spam and click "Verify registration".)
  - (Close the "Email Confirmed!" tab.)

[img]

## ⚙️ Configure Workspace

### ➡️ Permissions Tab
- user:
  - Call Management `off`
  - Create Public Channels `off`
  - Create Direct Messages `off`
  - Create Private Channels `off`
  - Create Team `off`
  - Delete Own Message `on` [as per ToS]
  - Start Discussion `off`
  - Start Discussion (Other-User) `off`
  - View History `on` [as per ToS]
  - View joined room `on` [as per ToS]

[img]

### ➡️ Settings Tab

#### ⬜ Accounts Block
- Allow Users to Delete Own Account `on` [as per ToS]
- Allow Username Change `off`

[img]

#### ⬜ Assets Bloc
All images provided based on images from:
- [gitlab.com/logo.svg](https://gitlab.com/open-map-community/landing-page/-/blob/main/static/logo.svg?ref_type=heads)
- [gitlab.com/favicon.ico](https://gitlab.com/open-map-community/landing-page/-/blob/main/static/favicon.ico?ref_type=heads)

[img]

#### ⬜ Layout Block

<details><summary>⚪ Custom CSS</summary>

###### Custom CSS
```css
/*For logged-out & logged-in*/
/* Danger button colour */

.rcx-button--primary-danger.hover, .rcx-button--primary-danger.is-hovered, .rcx-button--primary-danger:hover, .rcx-button--primary-danger { 
  color: white !important; 
}

/* Order roles nicely */
.message-body-wrapper button.user-card-message {
  order: -9999;
}

.message-body-wrapper time.time {
  order: 9999;
}

.message-body-wrapper span[data-role='he/him'],
.message-body-wrapper span[data-role='she/her'],
.message-body-wrapper span[data-role='they/them'],
.message-body-wrapper span[data-role='he/they'],
.message-body-wrapper span[data-role='she/they'],
.message-body-wrapper span[data-role='they/he'],
.message-body-wrapper span[data-role='they/she'],
.message-body-wrapper span[data-role='xe/xem'],
.message-body-wrapper span[data-role='ae/aer'],
.message-body-wrapper span[data-role='any pronouns'],
.message-body-wrapper span[data-role='it/its'],
.message-body-wrapper span[data-role='he/it'],
.message-body-wrapper span[data-role='it/he']{
  order: -1;
}

/*Fixing colours in Admin panel yet again*/

.rcx-table--sticky .rcx-table__cell--header {
  position: sticky;
  z-index: 10;
  top: 0;
  background-color: white;
  background-color: var(--color-dark) !important;
}

/******Big Emojis – 44 px is unchanged******/

.big > .emojione,
.message .emojione.big,
span.emoji.big {
  width: 60px;
  height: 60px;
}

/******Link Colour in Channels******/

body .main-content a {
  color: var(--color-blue) !important;
}

/*fix ping & mention role colours*/

.mention-link--me,
.message .mention-link--me,
.mention-link--group,
.message .mention-link--group {
  color: var(--mention-link-me-text-color) !important;
  background-color: var(--color-blue);
}

/******Home Code******/

section.page-home code {
  font-weight: normal;
  user-select: all;
}

section.page-home b {
  color: var(--main-color);
}

section.page-home b.large {
  font-size: 1.2rem;
}

section.page-home i {
  color: white !important;
}

section.page-home a {
  color: var(--color-blue) !important;
}

/*add line break to repetitive paragraphs*/

.body p + p {
  padding-top: 8.5px;
}
/****** Other time zone thing that is important for some reason ******/

.rcx-css-62c5ll {
  display: none !important;
}

/****** From Meeko ******/

/*full profiles, display full Status Message, and not just the first line.*/
.rcx-css-1lqct40 .rcx-css-14f881f + div {
  white-space: break-spaces;
}

/*full profiles, color headings*/
#react-root .rcx-css-1lqct40 > div:nth-child(4) > div > div:nth-child(1) {
  color: #e4609b !important;
}

/*full profiles, if roles not visible, hide time zones*/
/*hide time zones*/

.rcx-css-1lqct40 > div:nth-child(4) > div:nth-child(1):not(div[title]) > * {
  display: none !important;
}

/*do not hide roles title*/
.rcx-css-1lqct40 > div:nth-child(4) > div:nth-child(1):not(div[title]) > div:only-of-type,

/*do not hide roles names*/
.rcx-css-1lqct40 > div:nth-child(4) > div:nth-child(1):not(div[title]) > span:only-of-type {
  display: flex;
}

/****** Livestream Colours ******/

.rc-popout__controls,
.rc-popout__title,
rc-icon --plus rc-popout__close,
.rc-popout__close {
  color: #f2f3f5;
}

.rc-popout {
  min-width: 150px;
  max-width: 151px;
}

/****** Remove 'Delete' Button ******/
.rc-popover__divider,
li[data-id='report-message'] {
  display: none;
}

li[data-id='delete-message'] {
  border-top: #e1e5e8 solid 2px;
  margin-top: 16px;
  padding-top: 20px;
}

/****** Sidebar Bold Unread Text ******/
.rcx-sidebar-item__icon--highlighted,
.rcx-sidebar-item--highlighted {
  color: white;
  color: var(
    --rcx-sidebar-item-color-highlighted,
    var(--rcx-color-foreground-alternative, white)
  );
  font-weight: 700 !important;
}

/****** Sidebar width ******/

:root {
  --sidebar-width: 230px;
}

.rcx-sidebar-topbar__wrapper {
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-grow: 1;
  padding-block: 1rem;
  padding: 1rem;
  padding-inline: 1rem;
  padding-right: 0.2rem !important;
}

/****** Room Announcements ******/
.rcx-box {
  margin: 5em;
}

/****** Blogquote styling, because I want them ******/

blockquote {
  margin-top: 10px;
  margin-bottom: 10px;
  border-left: 3px solid var(--main-color);
}

/******Code text for donation on Home page ******/

.rc-old code {
  border-color: transparent;
}

/******Front page footer white text colour******
******I tried my best, cri******/

.login-terms,
.login-terms a {
  color: #fff !important;
}

div.powered-by,
a.color-tertiary-font-color {
  color: #fff !important;
  font-weight: normal !important;
  font-style: normal;
}

.rc-old #login-card a {
  color: #fff;
  text-decoration: underline;
  font-weight: normal;
}

/****** Home page login card ******/

.rc-old #login-card {
  margin-bottom: 0px;
  padding-bottom: 0px;
}

/******************************************
 *************Community Policy*************
 ******************************************/

.cms-page {
  width: 100%;
}

.cms-page h1,
.cms-page h2,
.cms-page h3,
.cms-page h4,
.cms-page h5,
.cms-page h6 {
  margin-bottom: 1em;
  color: var(--main-color);
}
.cms-page h1 {
  font-size: 2.3em;
}
.cms-page h2 {
  font-size: 2em;
}
.cms-page h3 {
  font-size: 1.6em;
}
.cms-page h4 {
  font-size: 1.4em;
}
.cms-page h5 {
  font-size: 1.2em;
}

.cms-page ol,
.cms-page ul,
.cms-page li,
.cms-page p {
  margin-left: 40px;
  margin-bottom: 14px;
  line-height: 24px;
}

.cms-page ol {
  list-style-type: decimal;
}
.cms-page ul {
  list-style-type: disc;
}

.cms-page a {
  color: var(--color-blue) !important;
}

/******************************************
   *********Our dark mode overrides**********
   ******************************************/

/*administration panel fix*/
.rcx-accordion-item__bar[tabindex].hover,
.rcx-accordion-item__bar[tabindex]:hover,
.rcx-accordion-item__bar:hover {
  background-color: var(--color-dark-medium) !important;
}

body {
  padding: 0 !important;
}

/* Fix sidebars */
.rcx-sidebar {
  background-color: var(--color-dark) !important;
}

.sidebar--custom-colors {
  --rcx-button-colors-ghost-background-color: #18181a !important;
}

.rc-old .code-colors {
  background-color: var(--color-dark-medium);
  color: var(--primary-font-color);
}

body .rcx-button {
  color: var(--main-color) !important;
}

:root {
  --primary-font-color: #444;
  --info-font-color: #a0a0a0;
  --color-darker: #1c1f25;
  --color-darkest: #18181a;
  --color-dark: #18181a;
  --color-dark-medium: #18181a;
  --color-blue: #47bac0;
  --color-dark-blue: #249aa0;

  --rcx-color-warning-200: var(--error-color);
  --rcx-color-surface: var(--color-darker);
  --rcx-color-foreground-default: var(--text-color);
  --rcx-color-neutral-100: var(--color-dark-70);
  --message-box-user-activity-color: var(--text-color);
  --rcx-tooltip-text-color: var(--text-color);

  --bg-color: #18181a;
  --main-color: #e4609b;
  --caret-color: #e4609b;
  --sub-color: #47bac0;
  --text-color: #fff;
  --error-color: #fff591;
  --error-extra-color: #b6af68;
  --colorful-error-color: #fff591;
  --colorful-error-extra-color: #b6af68;
}

/* Reset global font color so that it's changable more easily */
.color-primary-font-color,
textarea {
  color: var(--primary-font-color);
}

.color-info-font-color {
  color: var(--info-font-color);
}

input,
select,
textarea {
  color: var(--input-text-color);
}

.error-color {
  color: var(--rc-color-error);
}

.js-button[aria-label='Toggle Dark Mode'] {
  transition: filter 150ms;
}

.rcx-icon--name-darkmode {
  height: 1em;
  font-size: 1rem !important;
}

@media (min-width: 1372px) {
  .sidebar__toolbar-button {
    margin: 0 3px;
  }
}

@keyframes highlight {
  from {
    background-color: hsl(216, 92%, 54%);
  }
}

body {
  /****************************** Custom Variables ******************************/
  --primary-font-color: var(--color-gray-lightest);
  --info-font-color: var(--color-gray-light);
  --message-box-background: hsla(0, 0, 100%, 0.1);

  --button-outline-color: var(--color-gray-medium);
  --button-close-color: var(--color-gray-medium);

  /********************** Overridden Rocket.Chat Variables **********************/

  /* General Colors */
  --rc-color-alert-message-warning-background: hsl(352, 83%, 20%);
  --rc-color-primary: var(--color-gray-lightest);
  --rc-color-primary-lightest: var(--color-dark-medium);

  /* Forms - Button */
  --button-disabled-background: var(--color-dark-70);
  --button-disabled-text-color: var(--color-dark-80);

  /* Forms - Input */
  --input-text-color: var(--color-gray-lightest);
  --input-icon-color: var(--color-gray-lightest);

  /* Forms - popup list */
  --popup-list-background: var(--color-dark);
  --popup-list-background-hover: var(--color-darkest);
  --popup-list-selected-background: var(--color-dark);
  --popup-list-name-color: var(--color-white);

  /* Forms - tags */
  --tags-text-color: var(--color-white);
  --tags-background: var(--color-blue);

  /* Sidebar */
  --sidebar-background: var(--color-dark);
  --sidebar-background-hover: var(--color-darkest);

  /* Chip */
  --chip-background: var(--color-blue);

  /* Mention link */
  --mention-link-background: var(--color-dark-medium);
  --mention-link-text-color: var(--color-light-blue);
  --mention-link-me-background: var(--alerts-background);
  --mention-link-me-text-color: var(--color-white);
  --mention-link-group-background: var(--alerts-background);
  --mention-link-group-text-color: var(--color-white);

  /* Message box */
  --message-box-user-typing-color: var(--color-gray-lightest);
  --message-box-user-typing-user-color: var(--color-gray-lightest);

  /* Header */
  --header-title-username-color-darker: var(--color-gray-lightest);
  --header-background-color: var(--color-darkest);

  /* Popover */
  --popover-background: var(--color-dark);
  --popover-background-hover: var(--color-dark-medium);
  --popover-title-color: var(--color-white);
  --popover-item-color: var(--color-white);

  /* Tooltip */
  --tooltip-background: var(--color-darkest);
  --tooltip-text-color: var(--color-white);

  /* alert */
  --alerts-background: #1d73f5;
  --alerts-color: var(--color-white);

  --message-box-editing-color: var(--rc-color-alert-message-warning-background);
  --rc-color-alert: var(--color-dark-red);
}

/***************Main Chat*****************/

/* Breadcrumbs discussions */
body .rcx-room-header .rcx-tag--default {
  background-color: unset;
}

/* Blockquote */
body .rcx-css-1d5cod7 {
  background-color: var(--color-darkest) !important;
}

body blockquote.rcx-attachment__details .rcx-box--full {
  color: var(--secondary-font-color);
}

body .rcx-css-11c35pn:hover .rcx-attachment__details,
body .rcx-css-11c35pn:focus .rcx-attachment__details {
  background-color: var(--color-dark) !important;
}

/* Attachements content */
body .rcx-message-attachment .rcx-attachment__content .rcx-box--full {
  color: var(--primary-font-color);
}

body
  .rcx-message-attachment
  .rcx-attachment__content
  .rcx-box--full.rcx-box--with-block-elements
  pre
  code {
  background-color: var(--color-dark);
  color: var(--primary-font-color);
}

body
  .rcx-message-attachment
  .rcx-attachment__content
  .rcx-box--full.rcx-box--with-block-elements
  pre
  code {
  background-color: var(--color-dark);
  color: var(--primary-font-color);
}

/* Pinned messages content */
body .rcx-css-ntpg4f {
  color: var(--rc-color-primary) !important;
}

/* Message "You joined a new private conversation with" */
body .rcx-css-dlop43 {
  color: var(--rc-color-primary) !important;
}

body .main-content a {
  color: var(--color-light-blue);
}

body .main-content .messages-box .wrapper {
  background-color: var(--color-darkest);
}

body .mention-link--group {
  color: var(--mention-link-group-text-color) !important;
}

body .mention-link--me {
  color: var(--mention-link-me-text-color) !important;
}

body select {
  background-color: var(--color-dark);
}

body select option {
  color: var(--color-white);
}

body .sidebar-item > a {
  color: inherit;
}

body .highlight-text {
  background-color: var(--color-blue) !important;
}

body .rc-switch__text {
  color: var(--color-white);
}

body .rc-switch-double {
  color: var(--color-white);
}

body .rc-switch__button {
  background-color: var(--color-dark);
}

body .error-border {
  border-color: var(--color-dark-red);
}

body .background-component-color {
  background-color: var(--color-dark-blue);
}

body .upload-progress-progress {
  background-color: var(--color-blue);
}

body .container-bars .color-primary-action-color {
  color: var(--color-white);
}

body .burger i {
  background-color: var(--color-white);
}

body .rc-member-list__user.active,
body .rc-member-list__user:hover {
  background-color: var(--color-darkest);
}

body .rc-user-info-details {
  background-color: var(--color-dark-medium);
}

body p.rc-user-info-details__info {
  color: var(--color-white);
}

body .messages-container .footer,
body .content-background-color {
  background-color: var(--header-background-color);
}

body .message {
  background-color: var(--color-darkest);
}

body .message.new-day::after,
body .message .reactions > li,
body .border-component-color {
  border-color: var(--rc-color-primary-lightest);
}

body .message .reactions > li,
body .message .title .is-bot,
body .message .title .role-tag,
body .message.new-day::before {
  background-color: var(--rc-color-primary-dark);
  font-weight: 700;
}

body .message .reactions > li.selected {
  background-color: var(--rc-color-primary-dark);
}

body .message.active,
body .message:hover {
  background-color: var(--color-darker);
}

body .message.editing {
  background-color: var(--color-dark-blue);
}

body .message.first-unread .body:after {
  background-color: var(--header-background-color);
}

body .rc-message-box__container {
  background-color: var(--message-box-background);
}

body .rc-old .rc-message-box .reply-preview {
  background-color: var(--color-dark);
}

body .message-actions,
body .rc-member-list__counter {
  color: var(--color-gray-light);
  background-color: var(--color-darkest);
  border-color: var(--color-dark);
}

body .message-actions__button:hover,
body .message-actions__menu:hover {
  background-color: var(--color-dark);
}

body .message .body > table thead tr {
  background-color: var(--color-darkest);
}

body .message .body > table tr {
  background-color: var(--color-dark-medium);
}

body .message .body > table tr:nth-child(2n) {
  background-color: var(--color-dark);
}

body .background-transparent-darker-before::before {
  background-color: var(--color-dark-medium);
}

/* User card */
body .rcx-user-card {
  background-color: var(--color-dark) !important;
}

body .rcx-user-card .rcx-button:not(.rcx-css-ue04py) {
  background-color: var(--color-dark-medium);
  border: none;
}

body .rcx-user-card .rcx-button:hover {
  background-color: var(--color-dark-light);
  border: none;
}

/* Modals */

/*body .background-info-font-color {
  background-color: var(--color-dark-medium);
  }*/
body .rcx-modal__inner,
body .rcx-modal__footer {
  background: var(--color-dark);
}

body .rc-modal__footer,
body .rc-modal {
  background: var(--color-darkest);
}

body .rc-modal__content,
body .rc-modal__header,
body .rcx-modal__content,
body .rcx-modal__inner,
body .rcx-modal__header,
body .rcx-modal__title {
  color: var(--color-white);
}

body .rc-button--outline {
  border-color: var(--button-outline-color);
  color: var(--button-outline-color);
}

body .rc-button--outline.js-close,
body .rc-button--nude.js-close {
  border-color: var(--button-close-color);
  color: var(--button-close-color);
}

body .rc-button--cancel,
body .rc-button--danger {
  background-color: var(--button-cancel-color);
  border-color: var(--button-cancel-color);
  color: white;
}

body .contextual-bar {
  background-color: var(--color-dark);
  border-left: 2px solid var(--color-dark-medium);
}

body .contextual-bar__header {
  background-color: var(--color-dark);
  border-bottom: 1px solid var(--color-dark-medium);
}

body .contextual-bar__content {
  background-color: var(--color-dark);
}

body .rc-member-list__counter {
  background-color: var(--color-dark);
}

/**** Select / Dropdowns ****/

body .rcx-select {
  background-color: var(--rc-color-primary-darkest) !important;
}

body .rcx-options > .rcx-tile {
  background-color: var(--rc-color-primary-darkest) !important;
}

body .rcx-options .rcx-option,
  body .rcx-options .rcx-option--focus /* Temporary fix while focus is not refreshed */ {
  background-color: var(--rc-color-primary-darkest) !important;
  color: var(--color-white) !important;
}

body .rc-popover__content .rcx-option:hover,
  /* body .rcx-options .rcx-option--focus, */ /* Temporary fix while focus is not refreshed */
  body .rcx-options .rcx-option--selected {
  background-color: var(--color-dark-light) !important;
}

body .rcx-options .rcx-option:hover,
body .rcx-options .rcx-option--selected:hover {
  background-color: var(--color-dark) !important;
}

/***** Buttons *****/

/* Regular button style */
body .main-content .rcx-button:not(.rcx-button--ghost) {
  /* Default */
  background-color: var(--color-dark) !important;
}
body .main-content .rcx-button:is(.rcx-button--ghost, .rcx-button):hover {
  /* Hovered or selected */
  background-color: var(--color-darkest) !important;
}

/* Square (icon) button style */
body .main-content .rcx-button--square:not(.rcx-button--ghost), /* Default */
  body .main-content .rcx-button--square:is(.rcx-button--ghost, .rcx-button):hover {
  /* Hovered or selected */
  background-color: var(--color-darkest) !important;
  border-color: transparent !important;
}

body
  .main-content
  .rcx-button--square:is(.rcx-button--ghost, .rcx-button):focus {
  background-color: var(--color-darkest);
  border-color: transparent !important;
  box-shadow: 0 0 0 0.1rem var(--color-gray);
}

/* Menu buttons on top right (threads, search, etc.) */
body .rcx-css-15vvv6z:hover,
body .rcx-css-ue04py:hover,
body .rcx-css-15vvv6z:active,
body .rcx-css-ue04py:active {
  border-color: var(--color-dark-medium) !important;
  background-color: var(--color-dark-medium) !important;
}

body .rcx-button {
  color: var(--info-font-color);
}

body .rcx-button--ghost:not(.rcx-button--square):hover {
  color: var(--color-dark) !important;
}

body .rcx-button--primary {
  color: var(--info-font-color);
  background-color: #095ad2;
}

body .rcx-button--primary:disabled {
  color: var(--color-gray);
}

body .rcx-button--danger {
  color: white;
}

/***** Right sidebar *****/

/* TODO : switch toggle */

body .rcx-vertical-bar {
  background-color: var(--rc-color-primary-background) !important;
}

body .rcx-css-136xdpx:hover, /* Thread list item */
  body .rcx-css-136xdpx:focus, 
  body .rcx-css-1es44sn:hover, /* Files list item */
  body .rcx-css-1es44sn:focus {
  background-color: var(--rc-color-primary-dark);
}

/* Targets unread message indicator in threads panel. */
body button.rcx-contextual-message__follow + div.rcx-box--full {
  background-color: #1d74f5 !important;
}

/***** Chat file list *****/

body .attachments__item:hover,
.attachments__item:active {
  background-color: var(--color-darkest);
}

body .attachments__content:hover,
.attachments__content:active {
  color: var(--primary-font-color);
}

body .attachments__name {
  color: var(--color-blue);
}

body .attachments__name:hover,
.attachments__name:active {
  color: var(--color-light-blue);
}

body .rc-popover__content {
  background-color: var(--popover-background);
  box-shadow: 0px 0px 2px var(--color-dark-20);
}

body .emoji-picker .filter-item.active {
  border-color: var(--color-light-blue);
}

body .rcx-room-header hr.rcx-divider {
  border-color: var(--color-dark-medium);
}

body aside.rcx-box.rcx-box--full.rcx-vertical-bar,    /* right aside (threads, search, etc.) */
  body .rcx-css-ccvr3m,                                 /* thread list message */
  body .rcx-css-1j3nsmc,                                /* thread list message */
  body .rcx-css-1bmadou,                                /* thread list header */
  body .rcx-css-1yhzjdg                                 /* thread list search bar */ {
  border-color: var(--color-dark-medium) !important;
}

body .room-leader:hover {
  background-color: var(--color-darkest);
}

body .chat-now {
  color: var(--color-white);
}

body .message-popup-title {
  background-color: var(--color-dark);
}

/**************Code Highlights*****************/

body .code-colors,
body .rc-old code.inline {
  background: var(--color-dark-100);
  color: var(--color-gray-light);
}

body .hljs-selector-id,
body .hljs-keyword {
  color: var(--color-light-blue);
}

body .hljs-title {
  color: var(--color-gray-light);
}

body .hljs-literal,
body .hljs-number,
body .hljs-attr,
body .hljs-template-variable,
body .hljs-variable {
  color: var(--color-dark-green);
}

body .hljs-tag,
body .hljs-name {
  color: var(--color-light-blue);
}

body .hljs-selector-tag,
body .hljs-subst {
  color: var(--color-green);
}

body .hljs-doctag,
body .hljs-string {
  color: var(--color-red);
}

body .hljs-attribute,
body .hljs-type,
body .hljs-number {
  color: var(--color-orange);
}

body .hljs-addition {
  background-color: #1e3a21;
}
body .hljs-deletion {
  background-color: #472d2e;
}

/***** My Account *****/

body .rc-form-legend,
body .rc-form-label {
  color: var(--primary-font-color);
}

body .js-logout {
  color: var(--primary-font-color);
  border-color: var(--primary-font-color);
}

/************** Admin panel & Account panel ******************/

.page-list a:not(.rc-button),
.page-settings a:not(.rc-button) {
  color: var(--primary-font-color);
}

/*body .simplebar-content > .rcx-box => will also modify sidebar background */
body .simplebar-content > .rcx-css-fr02gd {
  /* main content */
  background-color: var(--color-dark);
}

body .rcx-css-txktj6 {
  /* Account settings page background */
  background-color: var(--color-dark) !important;
}

body .rcx-css-1wm5na {
  /* Account settings page header title */
  color: var(--primary-font-color) !important;
}

body .rc-scrollbars-container {
  /* Panels sidebar */
  background-color: var(--sidebar-background);
}

body .rcx-css-15hfnte {
  /* Panels sidebar header */
  background-color: var(--color-dark);
}

body .rcx-css-10ij0kz .rcx-box {
  /* Panels sidebar header text and button (with cross icon) */
  color: var(--primary-font-color) !important;
}

body .rcx-css-15hfnte .rcx-css-4pvxx3:hover {
  /* Panels sidebar button (with cross icon) hovered */
  color: var(--rc-color-primary-dark) !important;
}

body .rcx-css-1l00c5f,
body .rcx-css-1ky5rco {
  /* Panels sidebar item */
  color: var(
    --rcx-sidebar-item-color,
    var(--rcx-color-foreground-hint, #9ea2a8)
  ) !important;
}

body .rcx-css-1l00c5f.active {
  /* Panels sidebar item selected */
  background-color: rgba(108, 114, 122, 0.3);
}

body .rcx-css-1l00c5f:hover,
.rcx-css-1l00c5f:focus,
.rcx-css-1l00c5f.active:focus,
.rcx-css-1l00c5f.active:hover {
  /* Panels sidebar item hovered */
  background-color: var(--color-darkest);
}

body .rcx-css-kyq2rf {
  /* Admin panel info & stats (Deployment, License, Usage) */
  background-color: var(--color-dark) !important;
}

body .rcx-css-61di5s {
  /* Admin panel info & stats (Deployment, License, Usage) */
  color: var(--color-gray) !important;
}

body .rcx-select__item {
  color: var(--primary-font-color) !important;
}

body .sidebar-flex__header {
  background-color: var(--color-dark);
}

body .sidebar-light {
  background-color: var(--color-dark);
}

body .rcx-accordion-item__title,
body .rcx-label__text,
body .rcx-field__label {
  color: var(--color-white);
}

body .sidebar-flex__search .rc-input__element {
  color: var(--color-dark);
}

body .rcx-input-box__wrapper {
  background-color: var(--color-dark);
}

body .rcx-box * .rcx-input-box {
  background-color: var(--color-dark);
  color: var(--rc-color-primary);
}

body .rcx-table__cell {
  color: var(--color-gray) !important;
  background-color: var(--color-dark);
}

body .rcx-table__cell--header {
  color: var(--color-gray-lightest) !important;
}

body .rcx-table__cell--align-end {
  color: var(--color-gray);
  background-color: var(--color-gray);
}

body .rcx-css-18up6l1,
body .rcx-css-zvbm6,
body .rcx-css-n6qrb5 {
  /* Table cells content text*/
  color: var(--primary-font-color) !important;
}

body .rc-input__element:disabled {
  background-color: var(--color-gray);
}

body .admin-table-row {
  background-color: hsl(219, 16%, 25%);
}

body .sidebar-light .sidebar-item {
  color: inherit;
}

body .admin-table-row:nth-child(even) {
  background-color: hsl(219, 15%, 33%);
}

body .permissions-manager .permission-grid .id-styler {
  color: var(--info-font-color);
}

body .rcx-accordion-item__bar:hover {
  background-color: var(--color-dark-30);
}

body .rcx-box--text-style-h1,
body .rcx-subtitle,
body .rcx-box--text-color-default,
body .rcx-box--text-color-info {
  color: var(--color-gray-lightest);
}

body .permissions-manager .permission-grid .role-name {
  background: var(--color-dark);
}

body
  .rc-apps-marketplace
  .rc-table-content
  tbody
  .rc-table-tr:not(.table-no-click):not(.table-no-pointer):hover,
body
  .rc-apps-section
  .rc-table-content
  tbody
  .rc-table-tr:not(.table-no-click):not(.table-no-pointer):hover {
  background-color: var(--color-dark);
}

body
  .rc-apps-marketplace
  .rc-table-content
  .rc-table-info
  .rc-apps-categories
  .rc-apps-category,
body
  .rc-apps-section
  .rc-table-content
  .rc-table-info
  .rc-apps-categories
  .rc-apps-category {
  color: var(--primary-font-color);
  background-color: var(--color-dark-medium);
}

/*body .rcx-box * .rcx-input-box,*/
body .rcx-box * .rcx-select {
  /*color: var(--color-dark-medium);*/
  background-color: var(--color-white);
}

body .mail-messages__instructions {
  background-color: var(--color-dark);
}

body .rcx-tag--secondary {
  background-color: var(--color-dark-medium);
}

body .rcx-table__cell--align-end {
  color: var(--info-font-color) !important;
  background-color: var(--color-dark-medium) !important;
}

/* Apply info (white) font *everywhere* */
body .rcx-css-ps0pgs, /* Channel name */
  body .rcx-room-header  .rcx-box:not(.rcx-button-group):not(.rcx-button):not(.rcx-css-1fgkscl):not(.rcx-css-4pvxx3), /* omit buttons/icons (.rcx-css-1fgkscl is .rcx-icon parent) */
  body .rcx-vertical-bar .rcx-box:not(.rcx-button-group):not(.rcx-button):not(.rcx-css-1fgkscl):not(.rcx-css-4pvxx3):not(.rcx-css-trljwa) {
  color: var(--info-font-color) !important;
  /*background-color: var(--color-darkest) !important;*/
}

/* body .main-content .rcx-box {
    color: var(--info-font-color) !important;
  background-color: var(--color-dark) !important;
  } */

/* body .rcx-modal__backdrop {
  background-color: transparent !important;
  } */

body .rcx-table__cell--align-start {
  color: var(--info-font-color) !important;
  background-color: var(--color-dark-medium) !important;
}

body .rcx-field__description code {
  background-color: var(--color-dark);
}

body .table-fake-th {
  color: var(--info-font-color);
}

body .rc-input__element {
  background-color: var(--color-dark-medium);
  color: var(--info-font-color) !important;
}

body .rcx-check-box.is-focused,
body .rcx-check-box__input:checked + .rcx-check-box__fake,
body .rcx-check-box.is-focused,
body .rcx-check-box__input:indeterminate + .rcx-check-box__fake,
body .rcx-check-box__input:checked:focus + .rcx-check-box__fake,
body .rcx-check-box__input:indeterminate:focus + .rcx-check-box__fake,
body
  .rcx-radio-button.is-focused
  body
  .rcx-radio-button__input:checked
  + .rcx-radio-button__fake,
body .rcx-radio-button__input:checked:focus + .rcx-radio-button__fake,
body
  .rcx-toggle-switch.is-focused
  body
  .rcx-toggle-switch__input:checked
  + .rcx-toggle-switch__fake,
body .rcx-toggle-switch__input:checked:focus + .rcx-toggle-switch__fake {
  background-color: #1d74f5 !important;
}

body .CodeMirror {
  background-color: var(--color-gray-light);
}

body .CodeMirror-gutter {
  background-color: var(--color-dark);
}

body .setting-action {
  border: var(--button-border-width) solid var(--info-font-color);
  color: var(--info-font-color);
}

/**************Login Page******************/

body main#rocket-chat {
  background-color: var(--color-dark);
}

body section.full-page.color-tertiary-font-color {
  background-color: var(--color-dark);
}

body .rc-button.rc-button--nude.forgot-password,
body .rc-button.rc-button--nude.back-to-login,
body .rc-button.rc-button--nude.register,
body .rc-button.rc-button--nude i.icon-cancel,
body .register-link-replacement {
  color: var(--color-white);
}

body #login-card {
  background-color: var(--color-darkest);
}

/**************Scrollbars******************/
body .main-content *::-webkit-scrollbar {
  background-color: rgba(255, 255, 255, 0.05);
}

body .main-content *::-webkit-scrollbar-thumb {
  background-color: rgba(255, 255, 255, 0.15);
}

body .main-content *::-webkit-scrollbar-corner {
  background-color: rgba(255, 255, 255, 0.05);
}

/***** Poll App *****/
body [data-username='poll.bot'] .rcx-css-erwtrf {
  color: var(--primary-font-color) !important;
}

body *::-webkit-scrollbar {
  width: 0.75rem;
}

body *::-webkit-scrollbar-track {
  background-color: transparent;
}

body *::-webkit-scrollbar-thumb {
  background-color: #777;
}

/* Firefox does the dimming on hover automatically. We emulate it for Webkit-based browsers. */
body *::-webkit-scrollbar-thumb:hover {
  background-color: #666;
}

body *::-webkit-scrollbar-thumb:active {
  background-color: #444;
}

.dummy-entry {
  color: whitesmoke;
}

body .rcx-box--with-inline-elements code,
.rcx-field__description code,
.rcx-field__error code,
.rcx-field__hint code {
  background-color: var(--color-dark-100) !important;
  color: var(--primary-font-color) !important;
}

body figcaption.rcx-box.rcx-box--full.rcx-attachment__details {
  background-color: var(--color-darker) !important;
  color: var(--primary-font-color) !important;
}

span.rc-message-box__activity-user {
  color: var(--message-box-user-activity-color);
}
```
</details>

<details><summary>⚪ Custom Scripts</summary>

###### Custom Script for Logout Flow
```js
//logging-out, Empty
```

###### Custom Script for Logged Out Users
```js
/*logged-out, Empty*/
```

###### Custom Script for Logged In Users
```js
// Don't run any of this code if localStorage contains turnOffJsForTestingPurposes
//localStorage.setItem('turnOffJsForTestingPurposes', 'a')
if (localStorage.getItem('turnOffJsForTestingPurposes') != 'a')
{
  let isDOMreddyInterval = setInterval(() => {
    console.log('look for+++');
    goodDOM = document.querySelector('.main-content')
    if(goodDOM)
    {
      console.log('stop looking+++')
      clearInterval(isDOMreddyInterval);
      if (!goodDOM.classList.contains('dasia-vu'))
      {
        console.log('dasia-vu stop!!!')
        goodDOM.classList.add('dasia-vu')

        // Set default user preference
        // by default, the settings remove both the names and messages of blocked users
        // (Allowed values are: "messages only" and "everything")
        if(localStorage.getItem('preference') == null)
        {
          localStorage.setItem('preference', 'everything')
          console.log('=preference was null')
        }
        else
          console.log('=preference has value')

        function updateCSS() {
          // Find the location where we want to add the CSS
          let head = document.querySelector('head')

          // Look for the style element
          let styleElement = document.querySelector('.blocked-user-css')

          // If the CSS has not been added before
          if (!styleElement) {
            styleElement = document.createElement('style')
            // Give it a name to check if it exists later
            styleElement.classList.add('blocked-user-css')
            head.appendChild(styleElement)
          }
          // Decorate the blocked quote settings form
          styleElement.innerHTML = `
            .block-user-settings {
              width: 100%;
              padding: 20px;
              border-top: 2px solid;
            }
            .block-user-settings label {
              display: block;
              cursor: pointer;
            }`

          // If the user prefers to only hide the messages
          if (localStorage.getItem('preference') == 'messages only') {
            styleElement.innerHTML += `
              .quote--ignored > div:nth-child(2) {
                display: none;
              }`
          }

          // If the user prefers to hide the messages and names and everything
          if (localStorage.getItem('preference') == 'everything') {
            styleElement.innerHTML += `
              .message.message--ignored {
                min-height: 0;
                padding: 0;
              }
              .message--ignored button,
              .message--ignored button ~ div,
              .quote--ignored,
              .quote--revealed {
                display: none !important;
              }`
          }
        }
        // Update the CSS when the program first runs
        updateCSS()

        // Add settings form to page
        let blockedUser_AddSettingsObserver = new MutationObserver(() => {
          // If the page contains a list of dropdowns, and there are 7 dropdowns, assume this is the settings page
          if (
            document.querySelector('.rcx-css-a05vrv') && 
            document.querySelectorAll('.rcx-css-a05vrv .rcx-accordion-item').length >= 7
          )
          {
            // Get the location where we want the blocked quote settings to be placed
            let soundDropDown = document.querySelector('.rcx-css-a05vrv .rcx-accordion-item:last-child')
            // If the page does not have a settings form (div), then add it
            if (!soundDropDown.querySelector('.block-user-settings')) {
              let divElement = document.createElement('div')
              // Give the div a class to later check if it exists
              divElement.classList.add('block-user-settings')

              let textNode = document.createTextNode('Ignore user settings:')

              divElement.appendChild(textNode)

              // Create a radio button
              let inputElement = document.createElement('input')
              inputElement.type = 'radio'
              inputElement.name = 'blockuser'
              inputElement.value = 'messages only'

              // Make sure the button shows which setting is selected
              if (localStorage.getItem('preference') == 'messages only') {
                inputElement.checked = 'true'
              }

              let spanElement = document.createElement('span')
              spanElement.textContent = 'ignore messages only'

              let labelElement = document.createElement('label')
              labelElement.appendChild(inputElement)
              labelElement.appendChild(spanElement)

              divElement.appendChild(labelElement)

              let inputElement2 = document.createElement('input')
              inputElement2.type = 'radio'
              inputElement2.name = 'blockuser'
              inputElement2.value = 'everything'

              // Make sure the button shows which setting is selected
              if (localStorage.getItem('preference') == 'everything') {
                inputElement2.checked = 'true'
              }

              let spanElement2 = document.createElement('span')
              spanElement2.textContent = 'ignore everything'

              let labelElement2 = document.createElement('label')
              labelElement2.appendChild(inputElement2)
              labelElement2.appendChild(spanElement2)

              divElement.appendChild(labelElement2)

              // If a button is clicked
              divElement.addEventListener('change', (event) => {
                // Update the preference that the user selected
                localStorage.setItem('preference', event.target.value)
                updateCSS()
              })
              soundDropDown.appendChild(divElement)
            }
          }
        })
        blockedUser_AddSettingsObserver.observe(
          document.querySelector('.main-content'),
          { subtree: true, childList: true }
        )

        // Mark quotes for removal
        function updateClass() {
          // Create a temporary list of users we want to ignore
          let blockUsersArray = []

          // Go through the ignored messages on the page
          document.querySelectorAll('.message--ignored').forEach((message) => {
            // If the message author is not in the ignored list,
            if (!blockUsersArray.includes(message.dataset.username))
              // then add the message author to the ignored list
              blockUsersArray.push(message.dataset.username)
          })

          // Get all the quotes on the page
          document.querySelectorAll('blockquote.rcx-box').forEach((quote) => {
            // If the quote author is in our ignored list
            if (
              blockUsersArray.includes(
                quote.querySelector('.rcx-css-11l1wmo').innerHTML,
              )
            ) {
              // If the quote to ignore has not been revealed
              if (!quote.classList.contains('quote--revealed')) {
                // Add CSS class to hide the quote
                quote.classList.add('quote--ignored')
              }
            }
            // If the quote author is not in the ignored list,
            // and the quote is meant to be displayed
            else {
              // Remove class to make it visible
              quote.classList.remove('quote--ignored')
              // Make sure it can be hidden if the user blocks them
              quote.classList.remove('quote--revealed')
            }
          })
        }
        updateClass()

        let blockedUser_QuotesMarkerObserver = new MutationObserver(updateClass)
        blockedUser_QuotesMarkerObserver.observe(
          document.querySelector('.main-content'),
          { subtree: true, childList: true }
        )

        // Reveal hidden quotes when the quote author's name is clicked
        document.querySelector('.main-content').addEventListener('click', (event) => {
          // See if it's the author's name for a quote that was clicked
          let couldBeQuote = event.target.parentElement.parentElement
          // If it is the author's name of a blocked quote that is clicked on
          if (couldBeQuote.classList.contains('quote--ignored')) {
            // Stop hiding the quote
            couldBeQuote.classList.remove('quote--ignored')
            // Make sure to not hide it the next time a change to the DOM is detected
            couldBeQuote.classList.add('quote--revealed')
          }
        })
      }
    }
  }, 100);
}
```
</details>

<details><summary>⚪ Home page content</summary>

###### Home Title
`🚀Home`

###### Content block
```html
<br />
<b class="large">Welcome to Open MAP Community!</b>
<br />
<p><b>🔗 OMC's Links</b></p>
<p><a href="https://mapcommunity.org" target="_blank" rel="noopener">Home Website</a> (<a href="http://omchomelyytyjsfpbmfeumzuxbpd7svoktex5h4zcbuwrcmkfookhnqd.onion" target="_blank" rel="noopener">Onion</a>)</p>
<p><a href="https://blog.mapcommunity.org/omc/" target="_blank" rel="noopener">MAP Blogs</a> (<a href="http://mapblogg5mb5qcdrqmj3yjmbqh4hfyu7hgq6u5p5hqhlwm57lebbnyad.onion" target="_blank" rel="noopener">Onion</a>)</p>
<p><a href="https://chat.mapcommunity.org/" target="_blank" rel="noopener">OMC Chat</a> (<a href="http://omcchattwfr2z2xqjekt5yoatzg3f5dyiqzirz6pi73a3vmjy245scqd.onion" target="_blank" rel="noopener">Onion</a>)</p>
<p><b>🐤 Warrant Canary</b></p>
<p>If OMC's <a title="OMC's warrant canary" href="https://blog.mapcommunity.org/omc/warrant-canary" target="_blank" rel="noopener">warrant canary</a> expires or disappears, assume OMC has been compromised until it is updated or reappears. It expires and is renewed monthly on the first at 00:00 UTC.</p>
<p><b>💸 Donations</b></p>
<p>If you'd like to help keep our server up &amp; running, you may do so through many cryptocurrencies. Donations are greatly appreciated! 💜</p>
<p><b><i>Bitcoin</i></b>: <code>bc1qphurtewry2s5jfcskmukqvjthvejxwxu7sm0aj</code></p>
<p><b><i>Bitcoin Cash</i></b>: <code>qr4wg5d3cy2jsdu5zujpyvq0rvl7l6rxwunka0eer0</code></p>
<p><b><i>Dash</i></b>: <code>XspNe3Df7ebLQuDoBPygqupMre1EnNVLnU</code></p>
<p><b><i>Etherium</i></b>: <code>0x18ba269577af99449c323e8efd350ec40e807502</code></p>
<p><b><i>Litecoin</i></b>: <code>Li6A42pFrYAVbbgcEtV3Juz5kgeYPY2hXi</code></p>
<p><b><i>Monero</i></b>: <code>862rWgo6e8UH8haKevP1Tx11d5grzxsmsYJw3XF5SBzZhiGWekXJnyACJDCVCr7BfZcye9cpvbmzgHqE59BkLf1u3JgkKM7</code></p>
<p><b><i>ZCash</i></b>: <code>t1XUbrtdz2kZg8Q3Km95nFarRMKw3DsPTbG</code></p>
<p><b>📱 Mobile App</b></p>
<p>If you prefer to use a mobile app rather than use a browser to access this chat, Rocket Chat has apps for <a title="Rocket.Chat desktop apps" href="https://rocket.chat/download" target="_blank" rel="noopener">Desktop</a>, <a title="Rocket.Chat on the App Store" href="https://apps.apple.com/app/rocket-chat/id1148741252" target="_blank" rel="noopener">iOS</a> and <a title="Rocket.Chat on Google Play" href="https://play.google.com/store/apps/details?id=chat.rocket.android" target="_blank" rel="noopener">Android</a>. Once you download the Rocket Chat app, set the workspace URL to "<code>https://chat.mapcommunity.org</code>" and then log in with your username and password.</p>
<br />
<hr width="100%" />
<br />
<p>For further information about Rocket.Chat, consult the <a title="Rocket.Chat Documentation" href="https://rocket.chat/docs/" target="_blank" rel="noopener">Rocket.Chat documentation</a>.</p>
```
- Show custom content to homepage `off`

###### Terms of Service
```html
Our ToS is located at: <a href="https://mapcommunity.org/">mapcommunity.org</a>
```

###### Login Terms
```html
By proceeding you are agreeing to the OMC <a href="https://mapcommunity.org/#community-policy">Community Policy</a>
```

###### Privacy Policy
```html
Our ToS is located at: <a href="https://mapcommunity.org/">mapcommunity.org</a>
```

###### Legal Notice
```html
Our ToS is located at: <a href="https://mapcommunity.org/">mapcommunity.org</a>
```

###### Side Navigation Footer - Dark Theme
```html
<a class="dark" href="https://mapcommunity.org/#community-policy">
  <img src="assets/logo.png" alt="Logo" style="width: 24px; height: 24px;" />
  <span style="display: inline-block; line-height: 24px; overflow: auto; padding-left: 5px;">Read OMC Community Policy</span>
</a>
```

###### Side Navigation Footer
```html
<a class="light" href="https://mapcommunity.org/#community-policy">
  <img src="assets/logo.png" alt="Logo" style="width: 24px; height: 24px;" />
  <span style="display: inline-block; line-height: 24px; overflow: auto; padding-left: 5px;">Read OMC Community Policy</span>
</a>
```
</details>

<details><summary>⚪ Login</summary>

- Login Template `Vertical`
</details>

<details><summary>⚪ User Interface</summary>

- Use Real Name `on`
</details>